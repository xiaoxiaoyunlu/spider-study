package com.mano.demo;

import java.io.Serializable;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 15:00 2020/9/7
 * @Modified By:
 */
public class CrawlerUrl implements Serializable {


    private static final long serialVersionUID = 4333853185245757816L;

    public CrawlerUrl(){

    }

    private String oriUrl;  // 原始url   主机部分是域名
    private String url;  //  URL的值，主机部分是IP，为了防止重复主机的出现
    private int urlNo;   // URL   NUM


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getOriUrl() {
        return oriUrl;
    }

    public void setOriUrl(String oriUrl) {
        this.oriUrl = oriUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUrlNo() {
        return urlNo;
    }

    public void setUrlNo(int urlNo) {
        this.urlNo = urlNo;
    }
}
