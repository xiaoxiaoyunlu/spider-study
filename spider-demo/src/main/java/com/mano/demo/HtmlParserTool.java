package com.mano.demo;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.filters.OrFilter;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 13:14 2020/9/7
 * @Modified By:
 */
public class HtmlParserTool {

    public static Set<String> extractLinks(String url,LinkFilter filter){
        HashSet<String> links = new HashSet<>();

        try {
            Parser parser = new Parser(url);
            parser.setEncoding("gb2312");
            // 过滤<frame>标签里面的filter,用来提取frame标签里面的src属性
            NodeFilter frameFilter = new NodeFilter() {
                @Override
                public boolean accept(Node node) {
                    if(node.getText().startsWith("frame src=")){
                        return true;
                    }
                    return false;
                }
            };

            // OrFilter 来设置过滤<a>标签 和 <frame>标签
            OrFilter linkFilter = new OrFilter(new NodeClassFilter(LinkTag.class),frameFilter);

            // 得到所有过滤的标签
            NodeList list = parser.extractAllNodesThatMatch(linkFilter);
            for (int i = 0; i < list.size() ; i++) {
                Node tag = list.elementAt(i);
                // <a>标签
                if(tag instanceof LinkTag){
                    LinkTag link = (LinkTag)tag;
                    String linkUrl = link.getLink();
                    if(filter.accept(linkUrl)){
                        links.add(linkUrl);
                    }
                }else{
                    // 从<frame>标签 里提取src属性连接，如<frame src="test.html">
                    String frame = tag.getText();
                    int start = frame.indexOf("src=");
                    int end = frame.indexOf(" ");
                    if(end == -1){
                        end = frame.indexOf(">");
                    }
                    String frameUrl = frame.substring(5, end - 1);
                    if(filter.accept(frameUrl)){
                        links.add(frameUrl);
                    }
                }
            }


        } catch (ParserException e) {
            e.printStackTrace();
        }

        return links;
    }
}
