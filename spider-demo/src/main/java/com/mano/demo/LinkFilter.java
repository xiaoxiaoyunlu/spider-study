package com.mano.demo;

public interface LinkFilter {
    boolean accept(String frameUrl);
}
