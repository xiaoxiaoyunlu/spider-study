package com.mano.demo;

import java.util.Objects;
import java.util.Set;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 13:59 2020/9/7
 * @Modified By:
 */
public class MyCrawler {

    /**
     * 使用种子 初始化 队列
     * @param seeds
     */
    private void initCrawlerWithSeeds(String[] seeds){

        for (int i = 0; i < seeds.length; i++) {
            MyLinkedQueue.addUnVisitedUrl(seeds[i]);
        }
    }

    /**
     * 抓取过程
     * @param seeds
     */
    public void crawling(String[] seeds){
        // 定义过滤器，提取以 http://www.lietu.com开头的连接
        LinkFilter filter = new LinkFilter() {
            @Override
            public boolean accept(String frameUrl) {
                if(frameUrl.startsWith("http://www.lietu.com")){
                    return true;
                }
                return false;
            }
        };
        // 初始化队列
        initCrawlerWithSeeds(seeds);

        // 循环条件，待抓取的连接不为空，且抓取的网页不多于1000
        while(!MyLinkedQueue.unVisitedUrlsEmpty() && MyLinkedQueue.getVisitedUrlNum()<1000){
            // 队列头 URL出队
            String visitUrl = (String)MyLinkedQueue.unVisitedDequeue();
            if(Objects.isNull(visitUrl)){
                continue;
            }


            DownLoadFile downLoadFile = new DownLoadFile();
            // 下载网页
            downLoadFile.downLoadFile(visitUrl);
            // 添加到 已访问列表
            MyLinkedQueue.addVisitedUrl(visitUrl);
            // 提取出下载网页中的url
            Set<String> links = HtmlParserTool.extractLinks(visitUrl, filter);

            // 新的未访问url 入队
            for (String link:links) {
                MyLinkedQueue.addUnVisitedUrl(link);
            }

        }
    }


    public static void main(String[] args) {
        MyCrawler crawler = new MyCrawler();
        crawler.crawling(new String[]{"http://www.lietu.com"});

    }
}
