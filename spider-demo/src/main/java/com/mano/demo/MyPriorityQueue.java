package com.mano.demo;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 15:34 2020/9/4
 * @Modified By:
 */
public class MyPriorityQueue {

    // 已经访问的url集合
    private static Set visitedUrl = new HashSet();
    // 待访问的的url集合
    private static Queue unVisitedUrl = new PriorityQueue();

    public static Queue getUnVisitedUrl(){
        return unVisitedUrl;
    }


    public static void addVisitedUrl(String url){
        visitedUrl.add(url);
    }

    public static void removeVisitedUrl(String url){
        visitedUrl.remove(url);
    }

    public static Object unVisitedDequeue(){
        return unVisitedUrl.poll();
    }

    public static void addUnVisitedUrl(String url){
        if(url!=null && !url.trim().equals("")
                && !visitedUrl.contains(url)
                && ! unVisitedUrl.contains(url)){
            unVisitedUrl.add(url);
        }
    }

    public static  int getVisitedUrlNum(){
        return visitedUrl.size();
    }

    public static boolean unVisitedUrlsEmpty(){
        return unVisitedUrl.isEmpty();
    }
}
