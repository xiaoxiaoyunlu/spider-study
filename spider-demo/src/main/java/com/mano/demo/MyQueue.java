package com.mano.demo;

import java.util.LinkedList;

/**
 * @Author: zj
 * @Description:  URL 队列
 * @Date: Created in 15:31 2020/9/4
 * @Modified By:
 */
public class MyQueue {
    private LinkedList queue = new LinkedList();

    public void enqueue(Object obj){
        queue.addLast(obj);
    }

    public Object dequeue(){
        return queue.removeFirst();
    }

    public boolean isEmpty(){
        return queue.isEmpty();
    }

    public boolean isContain(Object obj){
        return queue.contains(obj);
    }


}
