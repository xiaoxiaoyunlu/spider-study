package com.mano.demo.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import java.io.IOException;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 9:33 2020/9/9
 * @Modified By:
 */
public class HDFSCatWithAPI {


    public static void main(String[] args) {
        // 指定 Configuration
        Configuration conf =  new Configuration();

        // 定义 DataInputStream
        FSDataInputStream in = null;

        try {
            // 得到文件系统的实例
            FileSystem fileSystem = FileSystem.get(conf);
            // 打开指定的文件
            in = fileSystem.open(new Path("hdfs://localhost:9000/user/myname/input/fixFrontsPath.sh"));
            // 将InputStream的内容 通过IOUtils 复制到 System.out中
            IOUtils.copyBytes(in,System.out,4096,false);
            // seek 到 position 1
            in.seek(1);

            // 执行一边复制一边输出工作
            IOUtils.copyBytes(in,System.out,4096,false);

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOUtils.closeStream(in);
        }
    }

}
