package com.mano.demo.hdfs;

import org.apache.hadoop.fs.FsUrlStreamHandlerFactory;
import org.apache.hadoop.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 9:25 2020/9/9
 * @Modified By:
 */
public class ReadDataFromHDFSDemo {

    static{
        // 啥意思啊 ？
        URL.setURLStreamHandlerFactory(new FsUrlStreamHandlerFactory());
    }

    public static void main(String[] args) {
        InputStream in = null;

        try {
            in = new URL("hdfs://127.0.0.1:9000/data/mydata").openStream();
            IOUtils.copyBytes(in,System.out,2048,false);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOUtils.closeStream(in);
        }
    }
}
