package com.mano.web.action;

import com.mano.web.config.XpaperWebmagicSchedulingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 16:53 2020/9/3
 * @Modified By:
 */

@RestController
@RequestMapping("/web")
public class GrabController {

    @Autowired
    private XpaperWebmagicSchedulingConfig config;

    @GetMapping("/test")
    public void test(){
        config.createLotteryInfo();
    }

}
