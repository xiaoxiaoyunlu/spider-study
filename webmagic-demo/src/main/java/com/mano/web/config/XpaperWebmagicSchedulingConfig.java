package com.mano.web.config;

import com.mano.web.webmagic.Downloader;
import com.mano.web.webmagic.XpaperZgtcbPopeline;
import com.mano.web.webmagic.XpaperZgtcbProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;

import javax.annotation.Resource;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 13:54 2020/9/3
 * @Modified By:
 */
@Component
public class XpaperWebmagicSchedulingConfig {

    private final Logger logger = LoggerFactory.getLogger(XpaperWebmagicSchedulingConfig.class);
    public static final String BASE_URL = "http://i.xpaper.net/cnsports";

    @Autowired
    private XpaperZgtcbPopeline xpaperZgtcbPopeline;

    /**
     * 中国体彩报 xpaper全媒体数字报 版面内容抓取
     * "0 0/1 18 * * ?" 每天18：00到18:59  没分钟执行一次
     *
     * "0 10 4 ? * *" 每天上午4:10触发
     */

    @Transactional
    @Scheduled(cron = "0 0/2 17 * * ?")
    public void createLotteryInfo(){

        System.out.println("中国体彩报 xpaper全媒体数字报 版面内容抓取");
        long startTime, endTime;
        System.out.println("【爬虫开始】");
        startTime = System.currentTimeMillis();

        HttpClientDownloader clientDownloader = Downloader.newIPDownLoader();

        logger.info("爬取地址：" + BASE_URL);

        try{

            Spider spider = Spider.create(new XpaperZgtcbProcessor());
            spider.addUrl(BASE_URL);
            spider.addPipeline(xpaperZgtcbPopeline);
            spider.setDownloader(clientDownloader);
            spider.thread(5);
            spider.setExitWhenComplete(true);
            spider.start();
            spider.stop();

        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        endTime = System.currentTimeMillis();
        System.out.println("【爬虫结束】");

        System.out.println("中国体彩报 xpaper全媒体数字报 版面内容抓取耗时约" + ((endTime - startTime) / 1000) + "秒，已保存到数据库.");
    }

}
