package com.mano.web.domain;

import java.util.Date;

/**
 * @Author: zj
 * @Description:
 * @Date: Created in 15:16 2020/9/3
 * @Modified By:
 */
public class JournalPage {

    private int id;
    private int journalId;
    private String pageHtmlTitle;
    private String pageHtmlUrl;
    private String pageHtmlDesc;
    private String pagePdfTitle;
    private String pagePdfUrl;
    private String pagePdfDesc;
    private String pageDesc;
    private short status;
    private Date grabDate;
    private Date createdAt;
    private Date updatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getJournalId() {
        return journalId;
    }

    public void setJournalId(int journalId) {
        this.journalId = journalId;
    }


    public String getPageHtmlDesc() {
        return pageHtmlDesc;
    }

    public void setPageHtmlDesc(String pageHtmlDesc) {
        this.pageHtmlDesc = pageHtmlDesc;
    }

    public String getPagePdfTitle() {
        return pagePdfTitle;
    }

    public void setPagePdfTitle(String pagePdfTitle) {
        this.pagePdfTitle = pagePdfTitle;
    }

    public String getPagePdfUrl() {
        return pagePdfUrl;
    }

    public void setPagePdfUrl(String pagePdfUrl) {
        this.pagePdfUrl = pagePdfUrl;
    }

    public String getPagePdfDesc() {
        return pagePdfDesc;
    }

    public void setPagePdfDesc(String pagePdfDesc) {
        this.pagePdfDesc = pagePdfDesc;
    }

    public String getPageDesc() {
        return pageDesc;
    }

    public void setPageDesc(String pageDesc) {
        this.pageDesc = pageDesc;
    }

    public String getPageHtmlTitle() {
        return pageHtmlTitle;
    }

    public void setPageHtmlTitle(String pageHtmlTitle) {
        this.pageHtmlTitle = pageHtmlTitle;
    }

    public String getPageHtmlUrl() {
        return pageHtmlUrl;
    }

    public void setPageHtmlUrl(String pageHtmlUrl) {
        this.pageHtmlUrl = pageHtmlUrl;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Date getGrabDate() {
        return grabDate;
    }

    public void setGrabDate(Date grabDate) {
        this.grabDate = grabDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
