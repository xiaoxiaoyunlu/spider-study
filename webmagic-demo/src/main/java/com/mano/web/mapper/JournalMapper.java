package com.mano.web.mapper;

import com.mano.web.domain.Journal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JournalMapper {
    Journal selectByIssue(String issue);

    void insertSelective(Journal journal);
}
