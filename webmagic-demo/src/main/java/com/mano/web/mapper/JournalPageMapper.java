package com.mano.web.mapper;

import com.mano.web.domain.JournalPage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JournalPageMapper {
    void insertSelective(JournalPage journalPage);
}
