package com.mano.web.webmagic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import java.util.Random;

/**
 * @Author: zj
 * @Description:自定义 HttpClientDownloader
 * @Date: Created in 10:01 2020/9/4
 * @Modified By:
 */
@Component
public class Downloader {

    private static RedisTemplate redisTemplate;

    @Autowired
    Downloader(RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    public static HttpClientDownloader newIPDownLoader(){
        return new HttpClientDownloader(){
            @Override
            protected void onError(Request request) {
                String[] ips = newIP();
                setProxyProvider(SimpleProxyProvider.from(new Proxy(ips[0],Integer.parseInt(ips[1]))));
            }
        };
    }

    /**
     * 获取 可用ip
     * @return
     */
     static String[] newIP() {
        Long size = redisTemplate.opsForList().size(UpdateIp.UPDATE_IP);
        String ip = redisTemplate.opsForList().index(UpdateIp.UPDATE_IP, new Random().nextInt(size.intValue())).toString();
        System.out.println("获取可用ip:"+ip);
        return ip.split(":");
    }

}
