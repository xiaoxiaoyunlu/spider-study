package com.mano.web;

import com.mano.web.config.XpaperWebmagicSchedulingConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WebmagicDemoApplicationTests {

    @Autowired
    private XpaperWebmagicSchedulingConfig xpaperWebmagicSchedulingConfig;

    @Test
    void contextLoads() {

        xpaperWebmagicSchedulingConfig.createLotteryInfo();

    }

}
